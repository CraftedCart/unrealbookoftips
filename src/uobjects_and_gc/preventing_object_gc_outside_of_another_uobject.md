# Preventing object GC outside of another UObject

Sometimes it can be handy to store `UObject` (or `FGCObject`) pointers in places where using `UPROPERTY()` to prevent garbage collection is unavailable. This can be done with the `TStrongObjectPtr` template.

```cpp
#pragma once

#include "UObject/StrongObjectPtr.h"

class FNotAUObject
{
private:
    TStrongObjectPtr<UTexture2D> AwesomeTexture;
};
```

```cpp
// Access members as normal
int32 TextureWidth = AwesomeTexture->GetSizeX();

// Get a UTexture2D* pointer as such
AwesomeTexture.Get()

// Check that the pointer is valid
AwesomeTexture.IsValid()

// Overwrite the pointer
AwesomeTexture.Reset(ADifferentTexture)

// Nullify the pointer
AwesomeTexture.Reset()

// Create a new strong object pointer
TStrongObjectPtr<UTexture2D>(AnotherTexture)
```

More info can be found on the API documentation: [https://docs.unrealengine.com/en-US/API/Runtime/CoreUObject/UObject/TStrongObjectPtr/index.html](https://docs.unrealengine.com/en-US/API/Runtime/CoreUObject/UObject/TStrongObjectPtr/index.html)

In addition, there is `TWeakObjectPtr`, which allows you to store a `UObject` without `UPROPERTY()`, however it does not increase the reference count. If no more strong references to a `UObject` exist, weak references will be set to `nullptr`.
