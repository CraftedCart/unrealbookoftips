# Pre-allocate memory for a container

## The short

Many containers in Unreal Engine 4 have a `.Reserve(SizeType Number)` function. There are generally no constructors for such containers that take an initial size.

## The long

If you need to add a large number of items to a container, it is generally a good idea to pre-allocate memory to contain such items. Typically when you add an item to a container, if there is not enough memory to fit the new item, the container will have to allocate a slightly larger region of memory than what it currently has; this may involve copying over all elements in the old bit of memory to the newly allocated bit of memory. As you may be able to imagine, adding large numbers of items into an array without first reserving space for the items can lead to a lot of re-allocations.

Many containers in Unreal Engine 4 have a `.Reserve(SizeType Number)` function that can be used to allocate enough memory to store `Number` amount of items.

### Example
```cpp
TArray<int32> ICanCount;
ICanCount.Reserve(1000);

for (int32 i = 0; i < 1000; i++)
{
    ICanCount.Add(i);
}
```
