# Using initializer lists

If you want to statically initialize a TMap, use `TPairInitializer`.

### Header
```cpp
extern TMap<FString, Type> PropertyLookupMap;
```

### Implementation
```cpp
TMap<FString, Type> PropertyLookupMap = {
    TPairInitializer<const FString&, const Type&>(TEXT("scalar"), Type::Scalar),
    TPairInitializer<const FString&, const Type&>(TEXT("vector2"), Type::Vector2),
    TPairInitializer<const FString&, const Type&>(TEXT("vector3"), Type::Vector3),
    TPairInitializer<const FString&, const Type&>(TEXT("texture2d"), Type::Texture2D),
};
```

Note how `TPairInitializer` uses const references in its template arguments, whereas the map definition does not.

---

`TPairInitializer` too unweildly for you? Here's a quick-n-dirty macro.

### Header
```cpp
    // In a class...
    static const TMap<const FTypeTag*, NewNodeCompFuncPtr> NODE_TYPE_NEW_MAP;
```

### Implementation
```cpp
#define MAP_PAIR TPairInitializer<const FTypeTag* const&, const NewNodeCompFuncPtr&>

const TMap<const FTypeTag*, NewNodeCompFuncPtr> AYourActor::NODE_TYPE_NEW_MAP = {
    MAP_PAIR(FTypeTag::From<FSceneNode>(), &URoSceneNodeComponent::FromSceneNode),
    MAP_PAIR(FTypeTag::From<FMeshNode>(), &URoMeshNodeComponent::FromSceneNode),
    MAP_PAIR(FTypeTag::From<FGoalNode>(), &URoGoalNodeComponent::FromSceneNode),
};

#undef MAP_PAIR
```
