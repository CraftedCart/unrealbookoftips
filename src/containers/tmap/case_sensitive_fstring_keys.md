# Case-sensitive FString keys

`FString::operator==` is case-insensitive by default for whatever reason. If you need a map whose keys are case sensitive, use `FLocKeyMapFuncs<V>`.

```cpp
TMap<FString, EThatThing, FDefaultSetAllocator, FLocKeyMapFuncs<EThatThing>> PropertyLookupMap;
```
