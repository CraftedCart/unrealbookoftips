# GetTypeHash() for custom TMap/TSet entries

Certain Unreal containers use hashing in their implementation to speed up access. If you want to use your own custom types with such containers (such as using a custom TMap key type), in addition to providing `operator==`, you'll need to provide a `GetTypeHash()` function too in the global namespace (not inside your class/struct).

One quick method you can use in some cases is `FCrc::MemCrc32` to hash an object.

```cpp
FORCEINLINE uint32 GetTypeHash(const FJoystickButtonInput& Object)
{
    return FCrc::MemCrc32(&Object, sizeof(FJoystickButtonInput));
}
```

---

Note that some objects that may appear equal may not always produce the same hash when using `MemCrc32`, for example an object that uses `FName`s.

```cpp
struct FUnrealKeyInput
{
    // FKey uses an FName internally
    FKey Key;

    bool operator==(const FUnrealKeyInput& Other) const
    {
        return Key == Other.Key;
    }

    // --snip--
};

FORCEINLINE uint32 GetTypeHash(const FUnrealKeyInput& Object)
{
    return Object.Key.GetFName().GetNumber();
}
```
