# The Unreal book of tips

The Unreal book of tips is a collection of useful snippets of code and bits of information for Unreal Engine 4 that I've generally found a bit tricky to search up and find around on the Internet. This book is aimed mostly towards programming in C++ for Unreal Engine (though this book may become more generalized in the future? ¯\\\_(ツ)\_/¯).

## Contributing

Have something handy you think may be worth sharing? You can contribute to this book at [https://gitlab.com/CraftedCart/UnrealBookOfTips](https://gitlab.com/CraftedCart/UnrealBookOfTips)! This book has been written in Markdown, using [mdBook](https://github.com/rust-lang/mdBook) to generate this website.

To add a page, create a .md file for it, and add a link to it in `src/SUMMARY.md` to get it to show up in the contents.
