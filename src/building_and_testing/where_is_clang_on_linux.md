# Where is clang/libcxx on Linux?

Unreal Engine does not use a system-wide installed `clang` to compile the engine and games, but rather downloads its own version. This can be found at...
```
UnrealEngine/Engine/Extras/ThirdPartyNotUE/SDKs/HostLinux/Linux_x64/*_clang-*/x86_64-unknown-linux-gnu/bin
```

Unreal Engine also uses its own libcxx - **if you're compiling third-party C++ code, you must compile/link against this libcxx.** Unreal's libcxx can be found at...
```
UnrealEngine/Engine/Source/ThirdParty/Linux/LibCxx
```

---

## An example third-party build script

To snip some lines from one of Rolled Out's build scripts...

(Note: This expects `UNREAL_PATH` and `INSTALL_DIR` variables to be set at some point - `CC` and `CXX` variables are also set to Unreal's `clang`/`clang++` before the script is ran)

```sh
UE_THIRD_PARTY_DIR=$UNREAL_PATH/Engine/Source/ThirdParty/

# --snip--

case "$(uname -s)" in
    Darwin*) # macOS
        THREADS="$(sysctl -n hw.ncpu)"
        CXX_FLAGS=""
        LINK_FLAGS=""
        ;;
    *) # Linux
        THREADS="$(grep -c ^processor /proc/cpuinfo)"
        CXX_FLAGS="-nostdinc++ -I $UE_THIRD_PARTY_DIR/Linux/LibCxx/include/ -I $UE_THIRD_PARTY_DIR/Linux/LibCxx/include/c++/v1"
        LINK_FLAGS="-nodefaultlibs -L $UE_THIRD_PARTY_DIR/Linux/LibCxx/lib/Linux/x86_64-unknown-linux-gnu/  $UE_THIRD_PARTY_DIR/Linux/LibCxx/lib/Linux/x86_64-unknown-linux-gnu/libc++.a $UE_THIRD_PARTY_DIR/Linux/LibCxx/lib/Linux/x86_64-unknown-linux-gnu/libc++abi.a -lm -lc -lgcc_s -lgcc"
        ;;
esac

# --snip--

cmake -G "Unix Makefiles"\
    -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS="${CXX_FLAGS}" \
    -DCMAKE_EXE_LINKER_FLAGS="${LINK_FLAGS}" \
    -DCMAKE_OSX_DEPLOYMENT_TARGET="10.11" \
    -DBUILD_SHARED_LIBS=FALSE \
    ..

make -j $THREADS
```
