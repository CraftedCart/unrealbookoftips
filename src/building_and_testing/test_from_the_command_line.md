# Test from the command line

In all these examples, I'm assuming your test names begin with `RolledOut` (For example, `RolledOut.StageConfigParser.ParseValid`) - they almost certainly don't. Replace `RolledOut` with whatever name your game's tests begin with.

These examples will also write out test results to a different log file, `TestResults.log`.

### Windows x64
```sh
.\UnrealEngine\Engine\Binaries\Win64\UE4Editor-cmd.exe ^
    "C:/full/path/to/your/game.uproject" ^
    "-execcmds=Automation RunTests RolledOut" ^
    -unattended ^
    -nopause ^
    "-testexit=Automation Test Queue Empty" ^
    -log=TestResults.log ^
    -game ^
    -nullrhi ^
    -nosound ^
    -nosplash
```

`Win32` works too in place of `Win64`.

For Powershell, the carets (`^`) need to be replaced with backticks (`` ` ``)... because reasons.

### macOS
```sh
./UnrealEngine/Engine/Binaries/Mac/UE4Editor.app/Contents/MacOS/UE4Editor \
    "/full/path/to/your/game.uproject" \
    "-execcmds=Automation RunTests RolledOut" \
    -unattended \
    -nopause \
    "-testexit=Automation Test Queue Empty" \
    -log=TestResults.log \
    -game \
    -nullrhi \
    -nosound \
    -nosplash
```

### Linux
```sh
./UnrealEngine/Engine/Binaries/Linux/UE4Editor \
    "/full/path/to/your/game.uproject" \
    "-execcmds=Automation RunTests RolledOut" \
    -unattended \
    -nopause \
    "-testexit=Automation Test Queue Empty" \
    -log=TestResults.log \
    -game \
    -nullrhi \
    -nosound \
    -nosplash
```
