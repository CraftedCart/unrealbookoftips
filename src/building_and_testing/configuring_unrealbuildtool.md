# Configuring UnrealBuildTool

UnrealBuildTool can be configured per-user, often times to help speed up compilation. This section of the book goes over some of the more common configuration options - a full list can be found on the Unreal docs, linked at the end of this section.

## The configuration file

UnrealBuildTool looks for a configuration file when running - this is where your preferences should go. If the file doesn't exist, you can create one with this template...
```xml
<?xml version="1.0" encoding="utf-8" ?>
<Configuration xmlns="https://www.unrealengine.com/BuildConfiguration">
  <BuildConfiguration>
    <!-- Your BuildConfiguration preferences go here -->
  </BuildConfiguration>

  <LocalExecutor>
    <!-- Your LocalExecutor preferences go here -->
  </LocalExecutor>

  <ProjectFileGenerator>
    <!-- Your ProjectFileGenerator preferences go here -->
  </ProjectFileGenerator>

  <!-- etc... -->
</Configuration>
```

The configuration file can be found at...

### Windows
```
%APPDATA%\Unreal Engine\UnrealBuildTool\BuildConfiguration.xml
Documents\Unreal Engine\UnrealBuildTool\BuildConfiguration.xml
```

Either location works for your configuration file.

### macOS or Linux
```
~/.config/Unreal Engine/UnrealBuildTool/BuildConfiguration.xml
~/Unreal Engine/UnrealBuildTool/BuildConfiguration.xml
```

Either location works for your configuration file.

## Disabling unity build

```xml
  <BuildConfiguration>
    <bUseUnityBuild>false</bUseUnityBuild>
  </BuildConfiguration>
```

By default, UnrealBuildTool batches up multiple cpp files into fewer, larger cpp files. This speeds up compilation when compiling files in batch, but can massively hurt iteration times as it still batches up files even if you make changes in just the one file. Disabling this causes each cpp file to be compiled separately.

Note that UnrealBuildTool *may* recompile the engine on your next build if you disable this.

## Using more or fewer threads to compile

```xml
  <LocalExecutor>
    <ProcessorCountMultiplier>2</ProcessorCountMultiplier>
    <MaxProcessorCount>16</MaxProcessorCount>
  </LocalExecutor>
```

`ProcessorCountMultiplier` can be used to multiply the number of threads UnrealBuildTool compiles with - this number can go below 1 if you want to use fewer threads.

`MaxProcessorCount` is a cap on the number of threads used to compile.

Either option can be used standalone, it is not required to have both options set if you don't need to.

## More info...

For more info on configuring UnrealBuildTool, see [https://docs.unrealengine.com/en-US/Programming/BuildTools/UnrealBuildTool/BuildConfiguration/index.html](https://docs.unrealengine.com/en-US/Programming/BuildTools/UnrealBuildTool/BuildConfiguration/index.html). This section of the book only covers the more common UnrealBuildTool options.
