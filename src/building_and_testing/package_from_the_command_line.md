# Package from the command line

### Windows x64
```sh
.\UnrealEngine\Engine\Build\BatchFiles\RunUAT.command ^
    BuildCookRun ^
    -project="C:/full/path/to/your/game.uproject" ^
    -noP4 ^
    -platform=Win64 ^
    -clientconfig=Shipping ^
    -serverconfig=Shipping ^
    -cook ^
    -allmaps ^
    -build ^
    -stage ^
    -archive
```

`Win32` works too in place of `Win64`.

For Powershell, the carets (`^`) need to be replaced with backticks (`` ` ``)... because reasons.

### macOS
```sh
./UnrealEngine/Engine/Build/BatchFiles/RunUAT.command \
    BuildCookRun \
    -project="/full/path/to/your/game.uproject" \
    -noP4 \
    -platform=Mac \
    -clientconfig=Shipping \
    -serverconfig=Shipping \
    -cook \
    -allmaps \
    -build \
    -stage \
    -archive
```

### Linux
```sh
./UnrealEngine/Engine/Build/BatchFiles/RunUAT.sh \
    BuildCookRun \
    -project="/full/path/to/your/game.uproject" \
    -noP4 \
    -platform=Linux \
    -clientconfig=Shipping \
    -serverconfig=Shipping \
    -cook \
    -allmaps \
    -build \
    -stage \
    -archive
```
