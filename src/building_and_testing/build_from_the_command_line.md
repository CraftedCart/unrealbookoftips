# Build from the command line

### Windows x64
```sh
.\UnrealEngine\Engine\Binaries\DotNET\UnrealBuildTool.exe ^
    Win64 ^
    Development ^
    -TargetType=Editor ^
    -Project="C:/full/path/to/your/game.uproject"
```

`Win32` works too in place of `Win64`.

For Powershell, the carets (`^`) need to be replaced with backticks (`` ` ``)... because reasons.

### macOS
```sh
mono ./UnrealEngine/Engine/Binaries/DotNET/UnrealBuildTool.exe \
    Mac \
    Development \
    -TargetType=Editor \
    -Project="/full/path/to/your/game.uproject"
```

### Linux
```sh
mono ./UnrealEngine/Engine/Binaries/DotNET/UnrealBuildTool.exe \
    Linux \
    Development \
    -TargetType=Editor \
    -Project="/full/path/to/your/game.uproject"
```

`Development` can be replaced with the configuration you want to build (`Debug`, `DebugGame`, `Development`, `Test`, `Shipping`).

`-TargetType=Editor` can be replaced with `-TargetType=Game`, or other target types.

More information on build configurations can be found at [https://docs.unrealengine.com/en-US/Programming/Development/BuildConfigurations/index.html](https://docs.unrealengine.com/en-US/Programming/Development/BuildConfigurations/index.html).
