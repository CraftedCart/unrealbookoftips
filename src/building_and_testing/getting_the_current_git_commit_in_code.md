# Getting the current git commit in code

You may find it helpful to define the current git commit as macros for C++, for example to show version information in your game. As UnrealBuildTool uses C# build scripts, you can essentially do whatever you like in your module build script, including running processes such as `git`. Do note that if you do this in your module build script, it may not always be up-to-date due to how UnrealBuildTool doesn't always run the build scripts if nothing has changed. This should be fine as long as the game is rebuilt on packaging however.

## YourModule.Build.cs

This build script defines the macros `PROJECT_VCS_VERSION_DESCRIPTION` and `PROJECT_VCS_COMMIT`. Example values for these macros may be `"f6e57f30-dirty"` and `"f6e57f30c649a16c86449d48d9d4eb95969dfd4d"` respectively.

```cs
public class YourModule : ModuleRules
{
    public YourModule(ReadOnlyTargetRules Target) : base(Target)
    {
        // --snip--

        // Define version strings
        DirectoryReference ProjectDir = Target.ProjectFile.Directory;
        string VcsDesc = GetGitDescription(ProjectDir);
        string VcsCommit = GetGitCommitHash(ProjectDir);

        PrivateDefinitions.Add(string.Format("PROJECT_VCS_VERSION_DESCRIPTION=\"{0}\"", VcsDesc));
        PrivateDefinitions.Add(string.Format("PROJECT_VCS_COMMIT=\"{0}\"", VcsCommit));
    }

    string GetProcessOutput(string ProcName, string ProcArgs)
    {
        var Proc = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = ProcName,
                Arguments = ProcArgs,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            }
        };

        Proc.Start();

        return Proc.StandardOutput.ReadToEnd();
    }

    string GetGitDescription(DirectoryReference Dir)
    {
        string EscapedDir = "\"" + Regex.Replace(Dir.FullName, @"(\\+)$", @"$1$1") + "\"";
        return GetProcessOutput("git", "-C " + EscapedDir + " describe --always --dirty --broken")
            .TrimEnd('\r', '\n');
    }

    string GetGitCommitHash(DirectoryReference Dir)
    {
        string EscapedDir = "\"" + Regex.Replace(Dir.FullName, @"(\\+)$", @"$1$1") + "\"";
        return GetProcessOutput("git", "-C " + EscapedDir + " rev-parse HEAD")
            .TrimEnd('\r', '\n');
    }
}
```

To get these as strings in C++, you can do...
```cpp
static const FString VCS_DESCRIPTION = TEXT(PROJECT_VCS_VERSION_DESCRIPTION);
static const FString VCS_COMMIT = TEXT(PROJECT_VCS_COMMIT);
```
