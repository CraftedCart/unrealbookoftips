# Adding pre-build steps

Pre-build steps can be configured in your .uproject file, in an appropriately named `PreBuildSteps` section.

Various properties can be used inside `PreBuildSteps`, and will be automatically expanded:
- `$(EngineDir)`
- `$(ProjectDir)`
- `$(TargetName)`
- `$(TargetPlatform)`
- `$(TargetConfiguration)`
- `$(TargetType)`
- `$(ProjectFile)`
- `$(PluginDir)`

```json
{
    "FileVersion": 3,
    "EngineAssociation": "{071F66E1-45CC-7869-EE18-68A4B500AF50}",
    "Category": "",
    "Description": "",
    "Modules": [
        {
            "Name": "YourModule",
            "Type": "Runtime",
            "LoadingPhase": "Default",
            "AdditionalDependencies": [
                "Engine",
                "ProceduralMeshComponent",
                "CoreUObject"
            ]
        }
    ],
    "Plugins": [
        // --snip--
    ],
    "PreBuildSteps":
    {
        "Win64": [
            "\"$(ProjectDir)\\Source\\ThirdParty\\build.bat\""
        ],
        "Mac": [
            "python3 '$(ProjectDir)/../build_project.py' third_party_only"
        ],
        "Linux": [
            "python3 '$(ProjectDir)/../build_project.py' third_party_only"
        ]
    }
}
```
