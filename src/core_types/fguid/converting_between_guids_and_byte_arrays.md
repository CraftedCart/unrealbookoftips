# Converting between GUIDs and byte arrays

Unreal Engine stores GUIDs as four `uint32`s for whatever reason. It can get a bit confusing if you need to convert between a GUID and bytes, such as for storage in an SQLite database or a save file, especially given that system endianness matters. Here's a couple of helper functions to convert between the two.

```cpp
namespace GuidUtils
{
    TArray<uint8> GuidToBytes(const FGuid& Guid)
    {
        const uint8* A = reinterpret_cast<const uint8*>(&Guid.A);
        const uint8* B = reinterpret_cast<const uint8*>(&Guid.B);
        const uint8* C = reinterpret_cast<const uint8*>(&Guid.C);
        const uint8* D = reinterpret_cast<const uint8*>(&Guid.D);

#if PLATFORM_LITTLE_ENDIAN
        return {
            A[3], A[2], A[1], A[0],
            B[3], B[2], B[1], B[0],
            C[3], C[2], C[1], C[0],
            D[3], D[2], D[1], D[0],
        };
#else
        return {
            A[0], A[1], A[2], A[3],
            B[0], B[1], B[2], B[3],
            C[0], C[1], C[2], C[3],
            D[0], D[1], D[2], D[3],
        };
#endif
    }

    FGuid GuidFromBytes(const TArray<uint8>& Bytes)
    {
#if PLATFORM_LITTLE_ENDIAN
        uint8 ScratchA[4];
        uint8 ScratchB[4];
        uint8 ScratchC[4];
        uint8 ScratchD[4];

        ScratchA[0] = Bytes[3];
        ScratchA[1] = Bytes[2];
        ScratchA[2] = Bytes[1];
        ScratchA[3] = Bytes[0];

        ScratchB[0] = Bytes[7];
        ScratchB[1] = Bytes[6];
        ScratchB[2] = Bytes[5];
        ScratchB[3] = Bytes[4];

        ScratchC[0] = Bytes[11];
        ScratchC[1] = Bytes[10];
        ScratchC[2] = Bytes[9];
        ScratchC[3] = Bytes[8];

        ScratchD[0] = Bytes[15];
        ScratchD[1] = Bytes[14];
        ScratchD[2] = Bytes[13];
        ScratchD[3] = Bytes[12];

        const uint32* A = reinterpret_cast<const uint32*>(&ScratchA);
        const uint32* B = reinterpret_cast<const uint32*>(&ScratchB);
        const uint32* C = reinterpret_cast<const uint32*>(&ScratchC);
        const uint32* D = reinterpret_cast<const uint32*>(&ScratchD);

        return FGuid(*A, *B, *C, *D);
#else
        const uint32* A = reinterpret_cast<const uint32*>(&Bytes[0]);
        const uint32* B = reinterpret_cast<const uint32*>(&Bytes[4]);
        const uint32* C = reinterpret_cast<const uint32*>(&Bytes[8]);
        const uint32* D = reinterpret_cast<const uint32*>(&Bytes[12]);

        return FGuid(*A, *B, *C, *D);
#endif
    }
}
```
