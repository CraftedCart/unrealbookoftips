# FGuid

A 128-bit **G**lobally **U**nique **ID**entifier, also sometimes referred to as a **U**niversally **U**nique **ID**entifier or UUID.
