# Summary

[The Unreal book of tips](./unreal_book_of_tips.md)

- [Building and testing](./building_and_testing/index.md)
  - [Build from the command line](./building_and_testing/build_from_the_command_line.md)
  - [Package from the command line](./building_and_testing/package_from_the_command_line.md)
  - [Test from the command line](./building_and_testing/test_from_the_command_line.md)
  - [Adding pre-build steps](./building_and_testing/adding_pre_build_steps.md)
  - [Configuring UnrealBuildTool](./building_and_testing/configuring_unrealbuildtool.md)
  - [Where is clang/libcxx on Linux?](./building_and_testing/where_is_clang_on_linux.md)
  - [Getting the current git commit in code](./building_and_testing/getting_the_current_git_commit_in_code.md)

- [Core types](./core_types/index.md)
  - [FGuid](./core_types/fguid/index.md)
    - [Converting between GUIDs and byte arrays](./core_types/fguid/converting_between_guids_and_byte_arrays.md)

- [UObjects and garbage collection](./uobjects_and_gc/index.md)
  - [Preventing object GC outside of another UObject](./uobjects_and_gc/preventing_object_gc_outside_of_another_uobject.md)

- [Containers](./containers/index.md)
  - [TMap](./containers/tmap/index.md)
    - [Using initializer lists](./containers/tmap/initializer_lists.md)
    - [Case-sensitive FString keys](./containers/tmap/case_sensitive_fstring_keys.md)
  - [Pre-allocate memory for a container](./containers/pre_allocate_memory_for_a_container.md)
  - [GetTypeHash() for custom TMap/TSet entries](./containers/get_type_hash_for_custom_keys.md)
